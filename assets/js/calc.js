Calc = Calc;

Calc.operators = ['+', '-', '*', '/', '(', ')'];
Calc.symbols = ['+', '-', '×', '÷'];

Calc.isOperator = function (key) {
  return Calc.operators.includes(key);
};

Calc.isMinus = function (key) {
  return Calc.symbols.indexOf(key) === 1;
};

Calc.isSymbol = function (key) {
  return Calc.symbols.includes(key);
};

Calc.findOperator = function (key) {
  return Calc.operators[(Calc.symbols.indexOf(key))];
};

Calc.inputEndWithOperator = function (operator) {
  var value = Calc.input.value.trim();
  if (!operator) {
    return Calc.isOperator(value.charAt(value.length - 1));
  } else {
    return value.endsWith('-');
  }
};

Calc.insertOperator = function (operator) {
  if (Calc.isMinus(operator) && (Calc.input.value.endsWith('e') || Calc.input.value.endsWith('E'))) {
    Calc.input.value += operator;

  } else if (Calc.isMinus(operator) && !Calc.inputEndWithOperator('-')) {
    Calc.input.value += ' ' + operator + ' ';

  } else if (!Calc.inputEndWithOperator()) {
    Calc.input.value += ' ' + operator + ' ';
  }
};

Calc.insertNumber = function (number) {
  Calc.input.value += number;
};

Calc.putDisplay = function (total) {
  document.getElementById('display').innerHTML += '<p>' + Calc.input.value + ' = ' + total + '</p>';
};

Calc.eval = function () {
  try {
    return eval(Calc.input.value.trim());
  } catch (e) {
    console.log(e);
    return e.toLocaleString().substr(0, e.toLocaleString().lastIndexOf(':'));
  }
};

Calc.resolve = function () {
  var total = Calc.eval();
  Calc.putDisplay(total);
  Calc.input.value = total;
};

Calc.back_s = function () {
  if (Calc.input.value.endsWith(' ')) {
    Calc.input.value = Calc.input.value.slice(0, Calc.input.value.length - 2);
  } else {
    Calc.input.value = Calc.input.value.slice(0, Calc.input.value.length - 1);
  }
};

Calc.move = function (controls, x, y) {
  controls.style.left = x + 'px';
  controls.style.top = y + 'px';
  controls.style.bottom = 'inherit';
};

document.onkeydown = function (keyboardEvent) {
  var key = keyboardEvent['key'];
  if (key === 'Enter') {
    keyboardEvent.preventDefault();
    Calc.resolve();
  }
};

document.onkeypress = function (keyboardEvent) {
  keyboardEvent.preventDefault();
  var key = keyboardEvent['key'];
  if (!isNaN(key) || Calc.isOperator(key) || ['e', 'E', '.'].includes(key)) {
    Calc.isOperator(key) ? Calc.insertOperator(key) : Calc.insertNumber(key);
  }
};

window.onload = function () {
  for (var x = 0; x <= 17; x++) {
    document.getElementById(x.toString()).onclick = function () {
      Calc.isSymbol(this.value) ? Calc.insertOperator(Calc.findOperator(this.value)) : Calc.insertNumber(this.value);
    };
  }

  document.getElementById('equal').onclick = function () {
    Calc.resolve();
  };

  document.getElementById('cancel').onclick = function () {
    document.getElementById('display').innerHTML = "";
    document.getElementById('input').value = "";
  };

  document.getElementById('back').onclick = function () {
    Calc.back_s();
  };

  document.getElementById('controls').ondragend = function (event) {
    Calc.move(this, event.clientX, event.clientY);
  };
};